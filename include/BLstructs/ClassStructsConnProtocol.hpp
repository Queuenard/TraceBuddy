#pragma once

/*
	NetConnection inherits from both ConnectionProtocol and SimGroup:
	class NetConnection : public ConnectionProtocol, public SimGroup

	The address of the single object depends on what a function expects.

	For example, any Sim-facing function (such as any working with the scripting engine) will see this object as a SimObject.

	In memory, the ConnectionProtocol section is located immediately before the SimObject section, like such:
	
	+0 bytes	ConnectionProtocol section
	Field 0		0x725d9c GameConnection::vftable
			Contains GameConnection-specific functions
	Field 1		Unknown

	...

	Field 39	Unknown


	+160 bytes	SimObject section
	Field 0		0x725e24 GameConnection::vftable
			Regular Representation as a SimObject
	...

*/

#define ClassStruct_NetConnection_to_ConnectionProtocol_NetConnection_offset -160
//Totals 156 bytes, exactly as written in engine/core/dnet.h:

#define ClassStruct_ConnectionProtocol \
	void* ConnectionProtocol_vftable; \
	unsigned int mLastSeqRecvdAtSend[32]; \
	unsigned int mLastSeqRecvd; \
	unsigned int mHighestAckedSeq; \
	unsigned int mLastSendSeq; \
	unsigned int mAckMask; \
	unsigned int mConnectSequence; \
	unsigned int mLastRecvAckAck; \
	bool mConnectionEstablished; \

struct ConnectionProtocol
{
	ClassStruct_ConnectionProtocol
};




//Start of object after SimGroup unknown
//Size before GameConnection unknown
//64 + ? bytes
#define ClassStruct_NetConnection \
	void* NetConnection_0[12]; \

struct NetConnection
{
	ClassStruct_SimObject
	ClassStruct_SimSet
	ClassStruct_SimGroup
	ClassStruct_NetConnection
};

#define ClassStruct_ConnectionProtocol_NetConnection \

struct ConnectionProtocol_NetConnection
{
	ClassStruct_ConnectionProtocol
	ClassStruct_ConnectionProtocol_NetConnection

	ClassStruct_SimObject
	ClassStruct_SimSet
	ClassStruct_SimGroup
	ClassStruct_NetConnection
};


/*
	Some of these fields may actually belong to NetConnection, however,
	GameConnection is the only inheritor of NetConnection.
*/

#define ClassStruct_GameConnection \
	int simtimeLastScopeQuery; /* CP+272, SO+112 */ \
	void* GameConnection_1[8]; \
	void** mSceneManager; /* CP+308, SO+148 (pointer to pointer to gClientContainer or gServerContainer) */ \
	void* GameConnection_2[10]; \
	unsigned int mCurRate_updateDelay; /* CP+352, SO+192 */ \
	unsigned int mCurRate_packetSize; /* CP+356, SO+196 */ \
	boolean mCurRate_changed; /* CP+360, SO+200 */ \
	unsigned int mMaxRate_updateDelay; /* CP+364, SO+204 */ \
	unsigned int mMaxRate_packetSize; /* CP+368, SO+208 */ \
	void* GameConnection_3[29]; \
	void* mGhostArray; /* CP+488, SO+328 */ \

struct GameConnection
{
	ClassStruct_SimObject
	ClassStruct_SimSet
	ClassStruct_SimGroup
	ClassStruct_NetConnection
	ClassStruct_GameConnection
};

struct ConnectionProtocol_GameConnection
{
	ClassStruct_ConnectionProtocol
	ClassStruct_ConnectionProtocol_NetConnection

	ClassStruct_SimObject
	ClassStruct_SimSet
	ClassStruct_SimGroup
	ClassStruct_NetConnection
	ClassStruct_GameConnection
};

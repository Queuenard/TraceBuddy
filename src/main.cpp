#include <windows.h>
#include <chrono>

//#define TSFUNCS_EXCLUDE_MINHOOK /* This program doesn't use any hooks */

#include "MinHook/MinHook.h"

#include "TSfuncs/TSFuncs.cpp"
#include "TSfuncs/TSHooks.cpp"
#include "TSfuncs/TSVector.cpp"

#include "BLstructs/include.hpp"

#include <fstream>
#include <list>

#define MAX_STACK_TIMING 1024

bool enableTrace = false;
bool enableFileDump = true;
int depth = 0;
unsigned int LIMIT_LINES = 12500;
std::list<std::string> trace_lines;


#define time_point std::chrono::time_point<std::chrono::high_resolution_clock>

time_point stack_start_times[MAX_STACK_TIMING];


void dump_to_file(bool terminating)
{
	if(terminating)
		printf("    - Dumping trace2 to file: TRACE_DUMP.log\n");
	else
		BlPrintf("    - Dumping trace2 to file: TRACE_DUMP.log");

	//dump buffer
	std::ofstream traceFile;
	traceFile.open("TRACE_DUMP.log", std::ios::out | std::ios::trunc);

	for(auto const& i : trace_lines)
		traceFile << i;

	traceFile.close();
}

void enableDisableTrace(bool enable, bool force_to_file, bool terminating)
{
	if(enable)
	{
		const char *simTime = tsf_BlCon__executef(1, "getSimTime");
		trace_lines.push_back("Trace started at sim time: "+ std::string(simTime) +"\n");
		BlPrintf("Trace2 (TraceBuddy) is enabled.");
		enableTrace = true;
	}
	else
	{
		trace_lines.push_back("Trace ended\n\n");
		BlPrintf("Trace2 (TraceBuddy) is disabled.");
		if(enableFileDump || force_to_file)
			dump_to_file(terminating);
		enableTrace = false;
	}
}

void TS_trace(ADDR obj, int argc, const char *argv[])
{
	bool prevTraceState = enableTrace;
	bool newTraceState = atoi(argv[1]) || stricmp(argv[1], "true") == 0;

	//Change in state
	if(prevTraceState != newTraceState)
		enableDisableTrace(newTraceState, 0, 0);
	else
	{
		if(enableTrace)
			BlPrintf("Trace2 (TraceBuddy) is already enabled.");
		else
			BlPrintf("Trace2 (TraceBuddy) is already disabled.");
	}
}

int TS_getLineCount(ADDR obj, int argc, const char *argv[])
{
	return trace_lines.size();
}

int TS_getLineLimit(ADDR obj, int argc, const char *argv[])
{
	return LIMIT_LINES;
}

void TS_setLineLimit(ADDR obj, int argc, const char *argv[])
{
	int new_limit = atoi(argv[1]);
	if(new_limit <= 0)
	{
		BlPrintf("trace2setLineLimit() - can't have limit less than 1");
		return;
	}
	
	LIMIT_LINES = new_limit;
}

int TS_popLines(ADDR obj, int argc, const char *argv[])
{
	int lines_to_remove = atoi(argv[1]);
	if(lines_to_remove <= 0)
	{
		BlPrintf("trace2popLines() - can't remove less than one line");
		return 0;
	}

	int lines_removed = 0;
	while(trace_lines.size() > 0 && lines_removed <= lines_to_remove)
	{
		trace_lines.pop_front();
		lines_removed++;
	}
	
	return lines_removed;
}

int TS_clear(ADDR obj, int argc, const char *argv[])
{
	int lines_removed = trace_lines.size();
	while(trace_lines.size() > 0)
		trace_lines.pop_front();

	return lines_removed;
}

void TS_flush(ADDR obj, int argc, const char *argv[])
{
	BlPrintf("Trace2 (TraceBuddy) flush requested.");
	dump_to_file(false);
}

void TS_file_dump_set(ADDR obj, int argc, const char *argv[])
{
	enableFileDump = (atoi(argv[1]) || stricmp(argv[1], "true") == 0) ? true : false;
}

void printTrace(bool entering, const char* retVal, const char* functionName, const char* packageName, Namespace* ns, int argc, const char** argv)
{
	if(!enableTrace)
		return;

	while(trace_lines.size() > LIMIT_LINES)
		trace_lines.pop_front();
	
	
	//if(!entering)
	//	depth--;


	std::string line_indent;

	for(int i = 0; i < depth; i++)
		line_indent += "\t";
		
	std::string line = line_indent;
	
	

	//This could happen if trace is initiated mid-function
	//Moving depth++ and depth-- (running even if trace2 is disabled) prevents this from happening
	if(depth < 0)
	{
		trace_lines.push_back("--Depth breakage due to callstack completing during trace2 enabling--\n");
		depth = 0;
	}
	
	if(entering)
	{
		if(depth < MAX_STACK_TIMING)
			stack_start_times[depth] = std::chrono::high_resolution_clock::now();
			
		//depth++;
	}
	

	//File is being executed
	if(!argv && functionName)
	{
		if(entering)
			line += "EXEC_ENTER: ";

		else
			line += "EXEC_LEAVE: ";

		line += functionName;

		trace_lines.push_back(line +"\n");
	}

	else
	{
		line += (entering ? "Entering " : "Leaving ");

		if(packageName)
		{
			line += "[";
			line += packageName;
			line += "]";
		}

		if(ns && ns->mName)
		{
			line += ns->mName;
			line += "::";
			line += functionName;
		}

		else if(functionName)
			line += functionName;

		line += "(";
		if(entering)
		{
			for(int i = 1; i < argc; i++)
			{
				line += "\"";
				line += argv[i];
				line += "\"";
				if(i != argc - 1)
					line += ", ";
			}

			line += ")";
		}

		else
		{
			line += ") - return ";
			line += retVal;
		}

		trace_lines.push_back(line +"\n");
	}
	
	if(!entering)
	{
		if(depth < MAX_STACK_TIMING)
		{
			std::string timing_line = line_indent;
			time_point now = std::chrono::high_resolution_clock::now();
			int diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(now-stack_start_times[depth]).count();
			timing_line += "   Time ms: "+ std::to_string(diff_ms);
			
			trace_lines.push_back(timing_line +"\n");
		}
		
		std::string simtime_line = line_indent;
		const char *simTime = tsf_BlCon__executef(1, "getSimTime");
		simtime_line += "   Sim time: "+ std::string(simTime);
		
		trace_lines.push_back(simtime_line +"\n");
	}
}

BlFunctionDef(const char*, __thiscall, CodeBlock__exec, CodeBlock*, int, const char*, Namespace*, int, const char**, bool, const char*, int);
BlFunctionHookDef(CodeBlock__exec);

const char* __fastcall CodeBlock__execHook(
	CodeBlock* obj,
	void *blank,
	int ip,
	const char* functionName,
	Namespace* ns,
	int argc,
	const char** argv,
	bool noCalls,
	const char* packageName,
	int setFrame)
{
	if(enableTrace)
		printTrace(true, NULL, functionName, packageName, ns, argc, argv);
		
	depth++;
	const char* ret = CodeBlock__execOriginal(obj, ip, functionName, ns, argc, argv, noCalls, packageName, setFrame);
	depth--;
	
	if(enableTrace)
		printTrace(false, ret, functionName, packageName, ns, argc, argv);

	return ret;
}


bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	//0x42fd20	CodeBlock::exec()
	BlScanFunctionHex(CodeBlock__exec, "53 8b dc 83 ec 08 83 e4 f0 83 c4 04 55 8b 6b 04 89 6c 24 04 8b ec 6a ff 68 ? ? ? ? 64 a1 00 00 00 00 50 53 81 ec b8 00 00 00");
	BlCreateHook(CodeBlock__exec);
	BlTestEnableHook(CodeBlock__exec);

	tsf_AddConsoleFunc(NULL, NULL, "trace2", TS_trace, "(bool enabled)", 2, 2);
	tsf_AddConsoleFunc(NULL, NULL, "trace2getLineCount", TS_getLineCount, "()", 1, 1);
	tsf_AddConsoleFunc(NULL, NULL, "trace2getLineLimit", TS_getLineLimit, "()", 1, 1);
	tsf_AddConsoleFunc(NULL, NULL, "trace2setLineLimit", TS_setLineLimit, "(int size)", 2, 2);
	tsf_AddConsoleFunc(NULL, NULL, "trace2popLines", TS_popLines, "(int amount)", 2, 2);
	tsf_AddConsoleFunc(NULL, NULL, "trace2clear", TS_clear, "()", 1, 1);
	tsf_AddConsoleFunc(NULL, NULL, "trace2flush", TS_flush, "() - flush to file without stopping trace (ignores file dump disabling)", 1, 1);
	tsf_AddConsoleFunc(NULL, NULL, "trace2useFile", TS_file_dump_set, "(bool enabled) - enable/disable dumping to file on trace end (default: on)", 2, 2);

	BlPrintf("    trace_lines pointer: 0x%p", &trace_lines);
	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");

	return true;
}

bool deinit(bool terminating)
{
	printf("%s: deinit'd\n", PROJECT_NAME);
	if(!terminating)
		BlTestDisableHook(CodeBlock__exec);

	if(!enableTrace)
	{
		printf("    - Trace was disabled\n");
		return true;
	}

	if(enableFileDump)
		dump_to_file(terminating);

	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, int reason, void *reserved)
{
	switch (reason)
	{
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit(reserved != NULL);
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}

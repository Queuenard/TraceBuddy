# Trace Buddy

Most of the credit belongs to hatf0 who made TraceEnabler, located here: https://github.com/hatf0/TraceEnabler

That project was hastily made to solve a problem for Conan's Farming. The goal in this project is to make a more usable trace function that doesn't bog down the console and is more controllable by script.

*To answer if there is "some added benefit from having the filestream open": yes, you should be keeping the stream open for successive writes as switching to/from kernel mode is expensive. Besides the time needed to write to disk and output to console, that is why the vanilla trace() function is extremely slow.*

This DLL adds a "trace2" function that dumps a custom (but extremely similar to regular trace) TorqueScript trace into a file if the program crashes\* or is requested to dump by script.

\* Windows does not appear to be capable of processing events on "surviving" threads as WINE on Linux does. A full memory dump (MDMP or "Small Memory Dump" file) must by analyzed to retrieve the stored trace log after a crash. At DLL load time, early in console.log, the address of the trace_lines list is printed out. A Python tool to read the trace log from a Windows memory dump has been released: https://notabug.org/Queuenard/blockland-memory-dump-search

When Blockland crashes on Linux with WINE, Blockland hangs in a post-crash "sleep" and I am able to use Ctrl+C (SIGINT) and this causes the shutdown code to run in TraceBuddy which dumps the trace to disk. Note that I intentionally break the path for winedbg.exe and intentionally prevent it from running:

`export WINEDLLOVERRIDES="winedbg.exe=fail"`

When the DLL is unloaded and "trace2" is enabled ahead of time, it dumps 12,500 lines (default) of trace into TRACE_DUMP.log.

Usage is similar to trace:

```cs
trace2(1) //Start storing trace log in memory
```

```cs
trace2(0) //Stop recording trace and dump to file immediately
```

New functions were added in versions 2 and 3 to control the internal state and limits:
```cs
trace2getLineCount() //Get amount of trace lines currently stored
```

```cs
trace2getLineLimit() //Get limit of trace lines stored in memory
```

```cs
trace2setLineLimit(); //Set limit of trace lines stored in memory; lowering the limit will delete lines whenever Trace Buddy is active and goes over the next function
```

```cs
trace2popLines(5); //Pop 5 lines off the stored trace lines; deleting large traces may take time.
```

```cs
trace2clear(); //Delete all trace lines stored in memory
```

```cs
trace2flush(); //Flush to file without stopping trace (ignores file dump disabling)
```

```cs
trace2useFile(1); //(bool enabled) - enable/disable dumping to file on trace end (default: on)
```

